<?php
/**
 * @file
 * CTools Export UI plugin for SexyBookmarks profiles.
 */

$plugin = array(
  'schema' => 'sexybookmarks_profiles',
  'access' => 'administer sexybookmarks',
  'menu' => array(
    'menu prefix' => 'admin/config/content',
    'menu item' => 'sexybookmarks',
    'menu title' => 'SexyBookmarks',
    'menu description' => 'Administer SexyBookmarks profiles.',
  ),
  'title singular' => t('profile'),
  'title singular proper' => t('Profile'),
  'title plural' => t('profiles'),
  'title plural proper' => t('Profiles'),
  'form' => array(
    'settings' => 'sexybookmarks_profiles_export_ui_form',
    'submit' => 'sexybookmarks_profiles_export_ui_form_submit',
  ),
);

/**
 * SexyBookmarks profiles settings form.
 */
function sexybookmarks_profiles_export_ui_form(&$form, &$form_state) {
  drupal_add_library('system', 'farbtastic');
 	drupal_add_js('jQuery(document).ready(jQuery("document").delegate("#replace_textfield_div","click", function(event){alert(jQuery(this).text());}));', array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
  $config = $form_state['item']->config;
  $form['config'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
		'#prefix' => '<div id="replace_config_div">',
    '#suffix' => '</div>',
  );
	drupal_add_css('.form-item-config-changethis {
     display:inline;margin-left: 20px;font-size: 15px;
    }
  ', 'inline');
	
	drupal_add_css('.form-item-config-classic-options {
     margin-left: 20px;font-size: 15px;
    }
  ', 'inline');
	
   $form['config']['changethis'] = array(
    '#title' => t("Choose the bookmarks style"),
		'#theme' => 'formfactors_select',	 
    '#type' => 'radios',
		'#default_value' =>isset($config['changethis']) ? $config['changethis'] : 1,
		'#parents' => array('config', 'changethis'),
    '#options' => array(
      1 => 'Sexybookmarks',
      2 => 'Classic Bookmarks',
			
    ),	 
  );
	 
	$form['config']['classic_options'] = array(
    '#title' => t("Choose the size"),
		'#theme' => 'classic_formfactor',	 
    '#type' => 'radios',
		'#default_value' => isset($config['classic_options']) ? $config['classic_options'] : 32,
		'#parents' => array('config', 'classic_options'),		
    '#options' => array(
      16 => '16X16',
      32 => '32X32',
			
    ),
    '#prefix' => '<div id="classic_div" style="margin-top: 30px;">',
    '#suffix' => '</div>',
			'#states' => array(
      'visible' => array(   // action to take.
        ':input[name="config[changethis]"]' => array('value' => 2),
      ),
    ),	
  );
	
	$form['config']['service'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($config['service']) ? $config['service'] : '5,7,88,2,3,257,74,52,207,191,236,54',
    '#theme' => 'sexybookmarks_services',
			'#prefix' => '<div id="replace_service_div">',
    '#suffix' => '</div>',
			'#states' => array(
      'visible' => array(   // action to take.
        ':input[name="config[changethis]"]' => array('value' => 1),
      ),
    ),	
  );

  // Functionality settings.
  $form['config']['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Functionality settings'),
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
		'#prefix' => '<div id="replace_settings_div">',
    '#suffix' => '</div>',
		'#states' => array(
      'visible' => array(   // action to take.
        ':input[name="config[changethis]"]' => array('value' => 1),
      ),
    ),	
  );
  $form['config']['settings']['fbLikeButton'] = array(
    '#type' => 'radios',
    '#title' => t('Include Facebook Like button'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#default_value' => isset($config['fbLikeButton']) ? $config['fbLikeButton'] : 0,
    '#parents' => array('config', 'fbLikeButton'),
  );
  $form['config']['settings']['fbButtonPos'] = array(
    '#type' => 'select',
    '#title' => t('Facebook Like button location'),
    '#options' => array(
      'bottom-left' => t('Bottom Left'),
      'bottom-right' => t('Bottom Right'),
      'top-left' => t('Top Left'),
      'top-right' => t('Top Right'),
    ),
    '#default_value' => isset($config['fbButtonPos']) ? $config['fbButtonPos'] : 'bottom-right',
    '#states' => array(
      'invisible' => array(
        'input[name="config[fbLikeButton]"]' => array('checked' => FALSE),
      ),
    ),
    '#parents' => array('config', 'fbButtonPos'),
  );
  $form['config']['settings']['dontShowShareCount'] = array(
    '#type' => 'radios',
    '#title' => t('Show Share counters'),
    '#options' => array(
      0 => t('Yes (recommended)'),
      1 => t('No'),
    ),
    '#default_value' => isset($config['dontShowShareCount']) ? $config['dontShowShareCount'] : 0,
    '#description' => t('For Facebook, Twitter, Google Buzz and Delicious'),
    '#parents' => array('config', 'dontShowShareCount'),
  );
  $form['config']['settings']['designer_toolTips'] = array(
    '#type' => 'radios',
    '#title' => t('Use Designer Tooltips'),
    '#options' => array(
      1 => t('Yes (recommended)'),
      0 => t('No'),
    ),
    '#default_value' => isset($config['designer_toolTips']) ? $config['designer_toolTips'] : 1,
    '#parents' => array('config', 'designer_toolTips'),
  );
  $form['config']['settings']['tip_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color for Tooltips'),
    '#size' => 7,
    '#maxlength' => 7,
    '#default_value' => isset($config['tip_bg_color']) ? $config['tip_bg_color'] : '#000000',
    '#states' => array(
      'invisible' => array(
        'input[name="config[designer_toolTips]"]' => array('checked' => FALSE),
      ),
    ),
    '#parents' => array('config', 'tip_bg_color'),
  );
  $form['config']['settings']['tip_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Color for Tooltips'),
    '#size' => 7,
    '#maxlength' => 7,
    '#default_value' => isset($config['tip_text_color']) ? $config['tip_text_color'] : '#FFFFFF',
    '#states' => array(
      'invisible' => array(
        'input[name="config[designer_toolTips]"]' => array('checked' => FALSE),
      ),
    ),
    '#parents' => array('config', 'tip_text_color'),
  );
  $form['config']['settings']['rel'] = array(
    '#type' => 'radios',
    '#title' => t('Add Nofollow to Links'),
    '#options' => array(
      'nofollow' => t('Yes'),
      '' => t('No'),
    ),
    '#default_value' => isset($config['rel']) ? $config['rel'] : 'nofollow',
    '#parents' => array('config', 'rel'),
  );
  $form['config']['settings']['target'] = array(
    '#type' => 'radios',
    '#title' => t('Open Links in New Window'),
    '#options' => array(
      '_blank' => t('Yes'),
      '_self' => t('No'),
    ),
    '#default_value' => isset($config['target']) ? $config['target'] : '_blank',
    '#parents' => array('config', 'target'),
  );
  $form['config']['settings']['shrlink'] = array(
    '#type' => 'radios',
    '#title' => t('Show Shareaholic Link'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#default_value' => isset($config['shrlink']) ? $config['shrlink'] : 1,
    '#parents' => array('config', 'shrlink'),
  );
  $form['config']['settings']['shortener'] = array(
    '#type' => 'select',
    '#title' => t('Which URL Shortener'),
    '#empty_value' => 'none',
    '#options' => array(
      'bitly' => t('bit.ly'),
      'google' => t('goo.gl'),
      'jmp' => t('jmp.li'),
      'supr' => t('su.pr'),
      'tinyurl' => t('tinyurl.com'),
    ),
    '#default_value' => isset($config['shortener']) ? $config['shortener'] : 'google',
    '#parents' => array('config', 'shortener'),
  );

  // Twitter specific settings.
  $form['config']['twitter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter settings'),
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
		'#prefix' => '<div id="replace_twitter_div">',
    '#suffix' => '</div>',
			'#states' => array(
      'visible' => array(   // action to take.
        ':input[name="config[changethis]"]' => array('value' => 1),
      ),
    ),	
  );
  $form['config']['twitter']['twitter_template'] = array(
    '#type' => 'textfield',
    '#title' => t('Configure Custom Tweet Template'),
    '#default_value' => isset($config['twitter_template']) ? $config['twitter_template'] : '${title} - ${short_link} via @Shareaholic',
    '#description' =>
      '<strong>' . t('Configuration Instructions') . ':</strong><br />'
      . '<em>' . t('Using the strings ${title} and ${short_link} you can fully customize your tweet output.') . '</em>'
      . '<p>'
      . '  <strong>' . t('Example Configurations') . ':</strong><br />'
      . '  <em>' . t('${title} - ${short_link} (via @Shareaholic)') . '</em><br />'
      . '  ' . t('or') . '<br />'
      . '  <em>' . t('RT @Shareaholic: ${title} - ${short_link}') . '</em>'
      . '</p>',
    '#parents' => array('config', 'twitter_template'),
  );

  // Aesthetic settings.
  $form['config']['aesthetics'] = array(
    '#type' => 'fieldset',
    '#title' => t('Aesthetic settings'),
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
		'#prefix' => '<div id="replace_aesthetics_div">',
    '#suffix' => '</div>',
		'#states' => array(
      'visible' => array(   // action to take.
        ':input[name="config[changethis]"]' => array('value' => 1),
      ),
    ),		
  );
  $form['config']['aesthetics']['expand'] = array(
    '#type' => 'radios',
    '#title' => t('Animate-expand multi-lined bookmarks'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#default_value' => isset($config['expand']) ? $config['expand'] : 1,
    '#parents' => array('config', 'expand'),
  );
  $form['config']['aesthetics']['positioning'] = array(
    '#type' => 'radios',
    '#title' => t('Auto-space/center the bookmarks'),
    '#options' => array(
      'spaced' => t('Space'),
      'center' => t('Center'),
      'no' => t('No'),
    ),
    '#default_value' => isset($config['spaced']) && $config['spaced'] ? 'spaced' : (
      isset($config['center']) && $config['center'] ? 'center' : 'no'
    ),
    '#parents' => array('config', 'positioning'),
  );
	if (!empty($form_state['values']['config']['changethis'])) {
		$form['config']['twitter2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter2 settings'),
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
		
		drupal_add_js('alert("Hello!");', array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
    $form['config']['replace_textfield']['#description'] = t("You have chosenpp '@value'", array('@value' => $form_state['values']['config']['changethis']));
  }

  
}

function ajax_example_simplest2_callback($form, $form_state) {
	
 return $form['config']['replace_textfield'];
}

/**
 * SexyBookmarks profiles settings form; submit callback.
 */
function sexybookmarks_profiles_export_ui_form_submit(&$form, &$form_state) {
  $form_state['values']['config']['center'] = $form_state['values']['config']['positioning'] == 'center';
  $form_state['values']['config']['spaced'] = $form_state['values']['config']['positioning'] == 'spaced';
  unset($form_state['values']['config']['positioning']);
}

function theme_formfactors_select($variables) {
	
  $form_element = $variables['form_element'];
	$default = $form_element['#default_value'];
	$checked1 ='checked="checked"';
	$checked2 ='checked="checked"';
	if($default==2)
		$checked1='';
	else
		$checked2='';
  
	return '<div class="form-item form-type-radios form-item-config-changethis" style="margin-top: 100px;">
 <div class="form-radios" id="edit-config-changethis"><div class="form-item form-type-radio form-item-config-changethis">
 <input type="radio" class="form-radio"'.$checked1.' value="1" name="config[changethis]" id="edit-config-changethis-1">  <label for="edit-config-changethis-1" class="option">Sexybookmarks </label>

</div>
<div class="form-item form-type-radio form-item-config-changethis" style="margin-left: 50px;">
 <input type="radio" class="form-radio"'.$checked2.' value="2" name="config[changethis]" id="edit-config-changethis-2">  <label for="edit-config-changethis-2" class="option">Classic Bookmarks </label>

</div>
</div>
</div>';
}

function theme_classic_formfactor($variables) {
  $form_element = $variables['form_element'];
	$default = $form_element['#default_value'];
	$checked1 ='checked="checked"';
	$checked2 ='checked="checked"';
	if($default==32)
		$checked1='';
	else
		$checked2='';
	return '<div id="classic_div"><div class="form-item form-type-radios form-item-config-classic-options" style="display: block;">
 <div class="form-radios" id="edit-config-classic-options"><div class="form-item form-type-radio form-item-config-classic-options">
 <input type="radio" class="form-radio"'.$checked1.'  value="16" name="config[classic_options]" id="edit-config-classic-options-1">  <img src="http://www.shareaholic.com/media/images/classicbookmark_16x16.png">

</div>
<div class="form-item form-type-radio form-item-config-classic-options">
 <input type="radio" class="form-radio"'.$checked2.'  value="32" name="config[classic_options]" id="edit-config-classic-options-2">  <img src="http://www.shareaholic.com/media/images/classicbookmark_32x32.png">

</div>
</div>
</div>
</div>';
}

function theme_sexybookmarks_services($variables) {
  $output = '';
  $form_element = $variables['form_element'];
  // List of SexyBookmarks services.
  $services = array(
    2, 3, 4, 5, 6, 7, 10, 24, 38, 39, 40, 52, 54, 74, 88, 201, 219, 257,
    27, 33, 41, 43, 45, 46, 53, 78, 195, 196, 205, 207, 210, 236, 247, 264,
    44, 48, 61, 77, 80, 100, 105, 140, 191, 202, 218, 238, 240, 266, 269, 280, 289,
    1, 14, 21, 89, 98, 102, 106, 243, 267, 268, 277, 278, 283, 284, 285, 286, 288,
    20, 59, 92, 188, 190, 192, 204, 246, 263, 265, 279, 281, 282, 287, 290, 291, 292, 293, 294,
  );
  $directory = md5(implode(',', $services));

  // Attach Javascript and Styles.
  $api = 'http://www.shareaholic.com/api/sprite/?v=2&apikey=' . SB_APIKEY . '&service=' . implode(',', $services);
  drupal_add_css(sexybookmarks_get_file($api, "{$directory}/sprite.css"));
  drupal_add_css('
    li.shareaholic {
      background-image: url("' . file_create_url(sexybookmarks_get_file("{$api}&apitype=png", "{$directory}/sprite.png")) . '");
    }

    div.shr-bookmarks ul.shr-socials li {
      cursor: move !important;
    }
  ', 'inline');
  drupal_add_js(drupal_get_path('module', 'sexybookmarks') . '/scripts/sexybookmarks.admin.js');
  drupal_add_library('system', 'ui.sortable');

  // Active services list.
  $active = array();
  foreach (explode(',', $form_element['#default_value']) as $item) {
    $active[] = array('data' => '', 'id' => "shr-{$item}", 'class' => array("shr-{$item}", 'shareaholic'));
  }
  $output .= '<div class="shr-bookmarks clearfix">' . theme('item_list', array('title' => t('Active services'), 'items' => $active, 'attributes' => array('id' => 'sexybookmarks-active', 'class' => array('shr-socials')))) . '</div>';

  // Inactive services list.
  $inactive = array();
  foreach (array_diff($services, explode(',', $form_element['#default_value'])) as $item) {
    $inactive[] = array('data' => '', 'id' => "shr-{$item}", 'class' => array("shr-{$item}", 'shareaholic'));
  }
  $output .= '<div class="shr-bookmarks clearfix">' . theme('item_list', array('title' => t('Inactive services'), 'items' => $inactive, 'attributes' => array('id' => 'sexybookmarks-inactive', 'class' => array('shr-socials')))) . '</div>';

  // Hidden field.
  element_set_attributes($form_element, array('id', 'name', 'value', 'size', 'maxlength'));
  $output .= theme('hidden', array('element' => $form_element));

  return $output;
}
